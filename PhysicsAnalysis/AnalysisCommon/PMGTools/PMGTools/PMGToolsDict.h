// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: PMGToolsDict.h 762199 2016-07-15 13:47:19Z aknue $
#ifndef PMGTOOLS_PMGTOOLSDICT_H
#define PMGTOOLS_PMGTOOLSDICT_H

// Local include(s):
#include "PMGTools/PMGSherpa22VJetsWeightTool.h"
#include "PMGTools/PMGCrossSectionTool.h"

#endif // PMGTOOLS_PMGTOOLSDICT_H
